1.2.1: Cleaned up code a bit
- Removed unused variables
- Define `aslist` and `power` outside of a table constructor
- Update `README.md`
  + I totally forgot about it for 1.2.0

---

1.2.0: Power sets and Cartesian product
- set.power for a power set
- set.list to convert a set to a list
- Cartesian product via `*` operator

---

1.1.0: General improvement
- Strict containment
- Hexversion
- Prefer Luaisms

---

1.0: Initial release

- Subtraction
- Union
- Intersection
- XOR
- Cardinality
- Equality
- Containment
- Adding elements
